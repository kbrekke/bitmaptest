﻿using Dapper;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BitmapTest
{
    internal class Program
    {
        private const string _connectionString = "Data Source=.;Initial Catalog=Artskart2Index;Integrated Security=True";
        private const string _directoryName = "Bitmaps";
        private const int _bandWidth = 4;
        private static ConcurrentQueue<List<TaxonCountForCell>> _queue = new ConcurrentQueue<List<TaxonCountForCell>>();
        private static int _processedCounter = 0;
        private static Stopwatch _stopwatch = new Stopwatch();
        private static bool _queueingDone = false;

        private static void Main(string[] args)
        {
            _stopwatch.Start();

            if (!Directory.Exists(_directoryName))
            {
                Directory.CreateDirectory(_directoryName);
            }

            int? N = null;//1000;
            int queuedCounter = 0;
            GridDimensions dimensions = null;
            const string dimensionsQuery =
                "SELECT MIN(row) AS MinRow, MIN(col) AS MinCol, MAX(row) AS MaxRow, MAX(col) AS MaxCol FROM ssb1km";
            const string countsQuery =
@"SELECT
	counts.[TaxonId]
	,counts.[Count]
	,ssb.row AS Row
	,ssb.col AS Col
  FROM [Artskart2Index].[dbo].[SsbGridTaxonCount] counts
  JOIN ssb1km ssb ON ssb.gid = counts.SsbGid
  ORDER BY TaxonId";

            var tasks = new List<Task>();
            
            using (var conn = new SqlConnection(_connectionString))
            {
                dimensions = conn.Query<GridDimensions>(dimensionsQuery).FirstOrDefault();
                
                // RGBA
                // Copy the RGB values into the array.
                //System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);

                //int byteCount = dimensions.MaxCol * dimensions.MaxRow * _bandWidth;
                //byte[] rgbValues = new byte[byteCount];

                var counts = conn.Query<TaxonCountForCell>(countsQuery, buffered: false);

                for (int i = 0; i < 7; i++)
                {
                    int workerId = i;
                    tasks.Add(Task.Run(() => Worker(dimensions, workerId)));
                }

                int currentTaxonId = -1;
                var currentCounts = new List<TaxonCountForCell>();

                foreach (var count in counts)
                {
                    if (currentTaxonId != count.TaxonId)
                    {
                        currentTaxonId = count.TaxonId;

                        if (currentCounts.Count == 0)
                        {
                            currentCounts.Add(count);
                            continue;
                        }

                        _queue.Enqueue(currentCounts.ToList());
                        queuedCounter++;

                        //if (HandleCounts(dimensions, bmp, rect, byteCount, rgbValues, currentCounts))
                        //{
                        //    counter++;
                        //}

                        //if (counter % 1000 == 0)
                        //{
                        //    Console.WriteLine($"Processed: { counter }, Elapsed: { sw.ElapsedMilliseconds } ms");
                        //}

                        currentCounts.Clear();

                        if (N.HasValue && queuedCounter == N)
                        {
                            break;
                        }
                    }

                    currentCounts.Add(count);
                }

                if(currentCounts.Count > 0)
                {
                    _queue.Enqueue(currentCounts.ToList());
                    queuedCounter++;
                }
            }

            _queueingDone = true;
            Task.WaitAll(tasks.ToArray());

            Console.WriteLine($"{ _processedCounter } bitmaps generated in { _stopwatch.ElapsedMilliseconds } ms.");
            Console.ReadKey();
        }

        private static async Task Worker(GridDimensions dimensions, int workerId)
        {
            Bitmap bmp = new Bitmap(dimensions.MaxCol, dimensions.MaxRow);

            Rectangle rect = new Rectangle(0, 0, bmp.Width, bmp.Height);

            int byteCount = dimensions.MaxCol * dimensions.MaxRow * _bandWidth;
            byte[] rgbValues = new byte[byteCount];
            
            while(true)
            {
                if(_queue.TryDequeue(out var counts))
                {
                    HandleCounts(dimensions, bmp, rect, rgbValues, counts);
                    Interlocked.Increment(ref _processedCounter);

                    if (_processedCounter % 1000 == 0)
                    {
                        Console.WriteLine($"Processed: { _processedCounter }, Elapsed: { _stopwatch.ElapsedMilliseconds } ms, WorkerId: { workerId }");
                    }
                }
                else
                {
                    if(_queueingDone)
                    {
                        break;
                    }
                    
                    await Task.Delay(25);
                }
            }
        }

        private static bool HandleCounts(GridDimensions dimensions, Bitmap bmp, Rectangle rect, byte[] rgbValues, List<TaxonCountForCell> currentCounts)
        {
            if (currentCounts.Count == 0)
            {
                return false;
            }

            BitmapData bmpData = bmp.LockBits(rect, ImageLockMode.ReadWrite, bmp.PixelFormat);

            // Get the address of the first line.
            IntPtr ptr = bmpData.Scan0;

            List<int> indexesToReset = new List<int>();

            foreach (var currentCount in currentCounts)
            {
                int row = (currentCount.Row - 1) * dimensions.MaxCol * _bandWidth;
                int columnStartIndex = row + ((currentCount.Col - 1) * _bandWidth);

                byte[] countBytes = BitConverter.GetBytes(currentCount.Count);

                for (int i = 0; i < countBytes.Length; i++)
                {
                    int index = columnStartIndex + i;
                    rgbValues[index] = countBytes[i];
                    indexesToReset.Add(index);
                }
            }

            System.Runtime.InteropServices.Marshal.Copy(rgbValues, 0, ptr, rgbValues.Length);

            bmp.UnlockBits(bmpData);

            using (FileStream compressedFileStream = File.Create($"{_directoryName}\\{currentCounts.First().TaxonId}.png.gz"))
            {
                using (GZipStream compressionStream = new GZipStream(compressedFileStream, CompressionMode.Compress))
                {
                    bmp.Save(compressionStream, ImageFormat.Png);
                }
            }

            //bmp.Save($"{_directoryName}\\{currentCounts.First().TaxonId}.png", ImageFormat.Png);

            for (int i = 0; i < indexesToReset.Count; i++)
            {
                rgbValues[indexesToReset[i]] = 0;
            }

            for (int i = 0; i < rgbValues.Length; i++)
            {
                if (rgbValues[i] != 0)
                {
                    throw new Exception("Not 0!");
                }
            }

            return true;
        }
    }

    public class GridDimensions
    {
        public int MinCol { get; set; }
        public int MinRow { get; set; }
        public int MaxCol { get; set; }
        public int MaxRow { get; set; }
    }

    public class TaxonCountForCell
    {
        public int TaxonId { get; set; }
        public int Count { get; set; }
        public int Row { get; set; }
        public int Col { get; set; }
    }
}